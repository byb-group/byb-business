package com.byb.utility;

import java.sql.*;
public class ConnectionUtility {
    static Connection conn;
    public static String error=null;
    public static Connection getConnection(){
            try{
                Class.forName("org.apache.derby.jdbc.ClientDriver");
                conn =DriverManager.getConnection("jdbc:derby://localhost:1527/UNIVERSITE","root","root");
                return conn;
            }
            catch (SQLException eSql) {
                error= "Erreur lors de la connexion à la base de données. Veuillez réessayer plus tard";
                return null;
            }
            catch (ClassNotFoundException eClass) {
                //error= "Driver non trouvé";
                error= "Erreur lors de la connexion à la base de données. Veuillez réessayer plus tard";
                return null;
            }
        }
    public static String getError() {
        return error;
    }
    
}