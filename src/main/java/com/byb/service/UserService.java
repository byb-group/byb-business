package com.byb.service;

import com.byb.dao.IUserDAO;
import com.byb.dao.UserDAO;
import com.byb.model.User;
import com.byb.test.JavaTest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class UserService {

    private Properties prop;
    private ArrayList<String> cassandraNodes;
    private IUserDAO userDAO;

    public UserService() {

        // Load our cluster config.
        prop = new Properties();

        try {
            prop.load(JavaTest.class.getResourceAsStream("/cluster.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cassandraNodes = new ArrayList<String>();
        cassandraNodes.add(prop.getProperty("nodes"));

        userDAO = new UserDAO(cassandraNodes,
                prop.getProperty("keyspace"));
    }

    public List<User> getAllUsers() {
        return userDAO.getAllUsersUsingString();
    }

    public User getUserByName(String username) {
        return userDAO.getUserByUsernameUsingPreparedStatement(username);
    }

    public String addUser(User user) {
        if (userDAO.getUserByUsernameUsingPreparedStatement(user.getUsername()) != null) {
            return "Username taken";
        } else {
            Date currentTimestamp = Calendar.getInstance().getTime();
            user.setCreated_date(currentTimestamp);
            userDAO.setUserByPreparedStatement(user);
            return "Done";
        }

    }

        public String updateUser(User user) {
        if (userDAO.getUserByUsernameUsingPreparedStatement(user.getUsername()) == null) {
            return "No User name found ";
        } else {
            Date currentTimestamp = Calendar.getInstance().getTime();
            user.setCreated_date(currentTimestamp);
            userDAO.setUserByPreparedStatement(user);
            return "Done";
        }

    }
        
    public User login(String username, String password) {

        User user = userDAO.getUserByUsernameUsingPreparedStatement(username);
        if (user == null) {
            return null;
        } else {
            if (user.getPassword().matches(password)) {
                return user;
            } else {
                return null;
            }
        }
    }
}
