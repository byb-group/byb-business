package com.byb.service;

import com.byb.dao.CommentDAO;
import com.byb.dao.ICommentDAO;
import com.byb.model.Comment;
import com.byb.test.JavaTest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class CommentService {

    private Properties prop;
    private ArrayList<String> cassandraNodes;
    private ICommentDAO commentDAO;

    public CommentService() {

        // Load our cluster config.
        prop = new Properties();

        try {
            prop.load(JavaTest.class.getResourceAsStream("/cluster.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cassandraNodes = new ArrayList<String>();
        cassandraNodes.add(prop.getProperty("nodes"));

        commentDAO = new CommentDAO(cassandraNodes,
                prop.getProperty("keyspace"));
    }

    public List<Comment> getCommentByUsername(String username) {
        return commentDAO.getCommentsByUsernameUsingPreparedStatement(username);
    }

    public List<Comment> getCommentByProjectId(UUID projectid) {
        return commentDAO.getCommentsByProjectIdUsingPreparedStatement(projectid);
    }

    public Comment addComment(Comment comment) {
        return commentDAO.setCommentByPreparedStatement(comment);
    }
}
