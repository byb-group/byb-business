package com.byb.service;

import com.byb.dao.IProjectDAO;
import com.byb.dao.ProjectDAO;
import com.byb.model.Project;
import com.byb.test.JavaTest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

public class ProjectService {

    private Properties prop;
    private ArrayList<String> cassandraNodes;
    private IProjectDAO projectDAO;

    public ProjectService() {

        // Load our cluster config.
        prop = new Properties();

        try {
            prop.load(JavaTest.class.getResourceAsStream("/cluster.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        cassandraNodes = new ArrayList<String>();
        cassandraNodes.add(prop.getProperty("nodes"));

        projectDAO = new ProjectDAO(cassandraNodes,
                prop.getProperty("keyspace"));
    }

    public List<Project> getAllProjects() {
        
        ArrayList<String> categorys = new ArrayList<String>();
        categorys.add("Art");
        categorys.add("Comics");
        categorys.add("Design");
        categorys.add("Fashion");
        categorys.add("Film");
        categorys.add("Games");
        categorys.add("Music");
        categorys.add("Publishing");
        categorys.add("Photography");
        categorys.add("Technology");
        categorys.add("Theater");
                   
        return projectDAO.getProjectsByCategorysUsingAsyncReadThreads(categorys);
    }
    
    public Project getProjectById(String projectid) {
        return projectDAO.getprojectByIdUsingQueryBuilder(projectid);
    }

    public List<Project> getProjectByUsername(String username) {
        return projectDAO.getProjectsByUsernameUsingAsyncRead(username);
    }
    
    public List<Project> getProjectByCategory(String category) {
        return projectDAO.getProjectsByCategoryUsingAsyncRead(category);
    }

    public Project addProject(Project project) {   
            return projectDAO.setProjectByPreparedStatement(project);
    }
    
    public String updateProject(Project project) {
        if (projectDAO.getprojectByIdUsingQueryBuilder(project.getProjectid().toString()) != null) {
            return "Project Id is taken";
        } else {
            projectDAO.setProjectByPreparedStatement(project);
            return "Done";
        }

    }

    public void backProject(UUID projectid, String username, int amount, int reward) {
        projectDAO.setBacking(projectid,username,amount);
    }


}
