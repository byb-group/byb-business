package com.byb.test;

import com.byb.dao.ProjectDAO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import com.datastax.driver.core.utils.UUIDs;
import com.byb.dao.CommentDAO;
import com.byb.model.Comment;
import com.byb.model.Project;
import com.byb.model.User;
import java.util.*;

public class JavaTest {

    public static void main(String[] args) {

        // Load our cluster config.
        Properties prop = new Properties();

        try {
            prop.load(JavaTest.class.getResourceAsStream("/cluster.properties"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        ArrayList<String> cassandraNodes = new ArrayList<String>();
        cassandraNodes.add(prop.getProperty("nodes"));

        ProjectDAO pdao = new ProjectDAO(cassandraNodes,
                prop.getProperty("keyspace"));

        Project p = new Project();
        p.setUsername("med");
        p.setBudjet(15000);
        p.setMinimum_pledge(100);
        p.setLocation("Tunisia");
        p.setDescription("This is a test project");
        Calendar cal = Calendar.getInstance();
        cal.setTime(Calendar.getInstance().getTime());
        cal.add(Calendar.DATE, 100); 
        p.setFunding_end_date(cal.getTime());
        p.setProjectname("Test Project");
        p.setCategory("Art");


        //pdao.setProjectByPreparedStatement(p);
        
        pdao.setBacking(UUID.fromString("ef744e80-e555-4700-b246-23ab5ba97b61"), "anis", 130);

        List<Project> projectList = pdao.getProjectsByCategoryUsingAsyncRead("Art");
        System.out.println(projectList.size());
        for (Project project : projectList) {
            System.out.println("Project" + project);

        }

        System.out.println("test");

        pdao.close();

        System.exit(0);
    }
}
