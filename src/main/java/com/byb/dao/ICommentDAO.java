/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byb.dao;

import com.byb.model.Comment;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author hadoop
 */
public interface ICommentDAO {

    void close();

    List<Comment> getCommentsByProjectIdUsingPreparedStatement(UUID projectid);

    List<Comment> getCommentsByUsernameUsingPreparedStatement(String username);

    Comment setCommentByPreparedStatement(Comment comment);
    
}
