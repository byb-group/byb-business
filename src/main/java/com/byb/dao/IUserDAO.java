/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byb.dao;

import com.byb.model.User;
import java.util.List;

/**
 *
 * @author hadoop
 */
public interface IUserDAO {

    void close();

    List<User> getAllUsersUsingString();

    /**
     * Simple example using a Prepared Statement.
     *
     * @param username
     * @return the user
     */
    User getUserByUsernameUsingPreparedStatement(String username);

    /**
     * Least secure method of executing CQL but here as an example.
     *
     * @param username
     * @return the user
     */
    User getUserByUsernameUsingString(String username);

    /**
     * Inserts a User by prepared statement. Sets the following fields:
     * username, firstname, lastname, email, password, created_date
     *
     * @param user
     */
    void setUserByPreparedStatement(User user);

    /**
     * Inserts a User by prepared statement. Sets the following fields:
     * username, firstname, lastname, email, password, created_date
     *
     * @param user
     */
    void setUserByUsingString(User user);
    
}
