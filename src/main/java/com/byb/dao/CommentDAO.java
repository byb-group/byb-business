package com.byb.dao;

import com.byb.model.Comment;
import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.Policies;
import java.util.*;

public class CommentDAO implements ICommentDAO {

    private static final String SET_COMMENT_BY_PROJECT = "   INSERT INTO comments_by_project (projectid,username,comment_ts, comment,projectname) VALUES (?,?,?,?,?)";
    private static final String SET_COMMENT_BY_USERNAME = "   INSERT INTO comments_by_user (projectid,username,comment_ts, comment,projectname) VALUES (?,?,?,?,?)";
    private static final String GET_COMMENT_BY_PROJECTID = "SELECT projectid,username,comment_ts, comment,projectname FROM  comments_by_project WHERE projectid = ?";
    private static final String GET_COMMENT_BY_USERNAME = "SELECT projectid,username,comment_ts, comment ,projectname FROM  comments_by_user WHERE username = ?";
    private final Cluster cluster;
    private final Session session;
    private final PreparedStatement setCommentByProject;
    private final PreparedStatement setCommentByUsername;
    private final PreparedStatement getCommentByUsername;
    private final PreparedStatement getCommentByProjectId;

    public CommentDAO(List<String> contactPoints, String keyspace) {

        cluster = Cluster.builder().addContactPoints(
                contactPoints.toArray(new String[contactPoints.size()])).withRetryPolicy(Policies.defaultRetryPolicy()).build();

        session = cluster.connect(keyspace);

        setCommentByProject = session.prepare(SET_COMMENT_BY_PROJECT);
        setCommentByUsername = session.prepare(SET_COMMENT_BY_USERNAME);
        getCommentByUsername = session.prepare(GET_COMMENT_BY_USERNAME);
        getCommentByProjectId = session.prepare(GET_COMMENT_BY_PROJECTID);


    }

    @Override
    public void close() {
        cluster.shutdown();
    }

    @Override
    public List<Comment> getCommentsByUsernameUsingPreparedStatement(
            String username) {

        BoundStatement bs = getCommentByUsername.bind(username);

        List<Comment> comments = new ArrayList<Comment>();

        for (Row row : session.execute(bs)) {
            Comment comment = new Comment();
            comment.setProjectid(row.getUUID("projectid"));
            comment.setUsername(row.getString("username"));
            comment.setComment_ts(row.getDate("comment_ts"));
            comment.setComment(row.getString("comment"));
            comment.setProjectName(row.getString("projectname"));

            comments.add(comment);
        }
        return comments;
    }

    @Override
    public List<Comment> getCommentsByProjectIdUsingPreparedStatement(UUID projectid) {

        BoundStatement bs = getCommentByProjectId.bind(projectid);
        List<Comment> comments = new ArrayList<Comment>();

        for (Row row : session.execute(bs)) {
            Comment comment = new Comment();
            comment.setProjectid(row.getUUID("projectid"));
            comment.setUsername(row.getString("username"));
            comment.setComment_ts(row.getDate("comment_ts"));
            comment.setComment(row.getString("comment"));
            comment.setProjectName(row.getString("projectname"));


            comments.add(comment);
        }

        return comments;
    }

    @Override
    public Comment setCommentByPreparedStatement(Comment comment) {

        BoundStatement bsP = setCommentByProject.bind();
        BoundStatement bsC = setCommentByUsername.bind();
        Date currentDate = Calendar.getInstance().getTime();
        comment.setComment_ts(currentDate);

        bsP.setString("username", comment.getUsername());
        bsP.setUUID("projectid", comment.getProjectid());
        bsP.setDate("comment_ts", comment.getComment_ts());
        bsP.setString("comment", comment.getComment());
        bsP.setString("projectname", comment.getProjectName());

        bsC.setString("username", comment.getUsername());
        bsC.setUUID("projectid", comment.getProjectid());
        bsC.setDate("comment_ts", comment.getComment_ts());
        bsC.setString("comment", comment.getComment());
        bsC.setString("projectname", comment.getProjectName());



        session.execute(bsP);
        session.execute(bsC);

        return comment;
    }
}
