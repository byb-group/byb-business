package com.byb.dao;

import com.byb.model.User;
import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.Policies;
import java.util.ArrayList;
import java.util.List;


public class UserDAO implements IUserDAO {

    private final Cluster cluster;
    private final Session session;
    private static final String GET_USER_BY_USERNAME = "SELECT * FROM users WHERE username = ?";
    private static final String SET_USER = "INSERT INTO users (username, firstname, lastname, email, password, location,created_date,biography) VALUES (?,?,?,?,?,?,?,?)";
    private final PreparedStatement getUserByNamePreparedStatement;
    private final PreparedStatement setUserByPreparedStatement;


    public UserDAO(List<String> contactPoints, String keyspace) {

        cluster = Cluster.builder().addContactPoints(
                contactPoints.toArray(new String[contactPoints.size()])).withRetryPolicy(Policies.defaultRetryPolicy()).build();

        session = cluster.connect(keyspace);

        getUserByNamePreparedStatement = session.prepare(GET_USER_BY_USERNAME);
        setUserByPreparedStatement = session.prepare(SET_USER);

    }

    @Override
    public void close() {
        cluster.shutdown();
    }

    @Override
    public List<User> getAllUsersUsingString() {

        List<User> users = new ArrayList<User>();


        ResultSet rs = session.execute("SELECT * FROM users");

        for (Row row : rs) {
            User user = new User();
            user.setUsername(row.getString("firstname"));
            user.setFirstname(row.getString("firstname"));
            user.setLastname(row.getString("lastname"));
            user.setEmail(row.getString("email"));
            user.setPassword(row.getString("Password"));
            user.setCreated_date(row.getDate("created_date"));
            user.setLocation(row.getString("location"));
            user.setBiography(row.getString("biography"));
            users.add(user);
        }

        return users;

    }

    /**
     * Least secure method of executing CQL but here as an example.
     *
     * @param username
     * @return the user
     */
    @Override
    public User getUserByUsernameUsingString(String username) {

        User user = null;

        ResultSet rs = session.execute("SELECT * FROM users WHERE username = '"
                + username + "'");

        // A result set has Rows which can be iterated over
        for (Row row : rs) {
            user = new User();
            user.setUsername(username);
            user.setFirstname(row.getString("firstname"));
            user.setLastname(row.getString("lastname"));
            user.setEmail(row.getString("email"));
            user.setPassword(row.getString("Password"));
            user.setCreated_date(row.getDate("created_date"));
            user.setLocation(row.getString("location"));
            user.setBiography(row.getString("biography"));
        }

        return user;

    }

    /**
     * Simple example using a Prepared Statement.
     *
     * @param username
     * @return the user
     */
    @Override
    public User getUserByUsernameUsingPreparedStatement(String username) {

        User user = null;

        // The BoundStatement is created by the PreparedStatement created above
        BoundStatement bs = getUserByNamePreparedStatement.bind();
        bs.setString("username", username);

        // Custom retry policy

        ResultSet rs = session.execute(bs);

        for (Row row : rs) {
            user = new User();
            user.setUsername(username);
            user.setFirstname(row.getString("firstname"));
            user.setLastname(row.getString("lastname"));
            user.setEmail(row.getString("email"));
            user.setPassword(row.getString("password"));
            user.setCreated_date(row.getDate("created_date"));
            user.setLocation(row.getString("location"));
            user.setBiography(row.getString("biography"));
        }

        return user;

    }

    /**
     * Inserts a User by prepared statement. Sets the following fields:
     * username, firstname, lastname, email, password, created_date
     *
     * @param user
     */
    @Override
    public void setUserByPreparedStatement(User user) {

        BoundStatement bs = setUserByPreparedStatement.bind();
        bs.setString("username", user.getUsername());
        bs.setString("firstname", user.getFirstname());
        bs.setString("lastname", user.getLastname());
        bs.setString("email", user.getEmail());
        bs.setString("password", user.getPassword());
        bs.setDate("created_date", user.getCreated_date());
        bs.setString("location", user.getLocation());
        bs.setString("biography", user.getBiography());
        session.execute(bs);

    }

    /**
     * Inserts a User by prepared statement. Sets the following fields:
     * username, firstname, lastname, email, password, created_date
     *
     * @param user
     */
    @Override
    public void setUserByUsingString(User user) {

        StringBuffer userInsert = new StringBuffer(
                "INSERT INTO users (username, firstname, lastname, email, password, location, created_date,biography) VALUES (");
        userInsert.append("'" + user.getUsername() + "'");
        userInsert.append("'" + user.getFirstname() + "'");
        userInsert.append("'" + user.getLastname() + "'");
        userInsert.append("'" + user.getEmail() + "'");
        userInsert.append("'" + user.getPassword() + "'");
        userInsert.append("'" + user.getLocation() + "'");
        userInsert.append("'" + user.getCreated_date().toString() + "'");
        userInsert.append("'" + user.getBiography() + "'");

        userInsert.append(")");

        session.execute(userInsert.toString());

    }

}
