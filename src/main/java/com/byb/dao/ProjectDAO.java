package com.byb.dao;

import com.byb.model.Project;
import com.datastax.driver.core.*;
import com.datastax.driver.core.policies.Policies;
import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;
import static com.datastax.driver.core.querybuilder.QueryBuilder.select;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ProjectDAO implements IProjectDAO {

    private static final Object PRESENT = new Object();
    private static final String GET_PROJECTS_BY_USERNAME = "SELECT projectid FROM username_projects_index WHERE username = ?";
    private static final String GET_PROJECT_BY_ID = "SELECT * FROM projects WHERE projectid = ?";
    private static final String GET_PROJECTS_BY_CATEGORY = "SELECT * FROM category_projects_index WHERE category = ?";
    private static final String GET_COUNTERS_BY_PROJECT = "SELECT views_counter, comments_counter FROM  project_counters WHERE projectid = ?";
    private static final String SET_PROJECT = "INSERT INTO projects (projectid,projectname,username,location,launched_date,funding_end_date,budjet,minimum_pledge,description,category) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static final String SET_CATEGORY_PROJECTS_INDEX = "INSERT INTO category_projects_index (category, projectid) VALUES (?,?)";
    private static final String SET_USERNAME_PROJECTS_INDEX = "INSERT INTO username_projects_index (username, projectid,launched_date) VALUES (?,?,?)";
    private static final String SET_BACKING_USER = "INSERT INTO user_backing_index (username, projectid,pledge_amount) VALUES (?,?,?)";
    private final Cluster cluster;
    private final Session session;
    private final PreparedStatement getProjectsByUsernamePreparedStatement;
    private final PreparedStatement getProjectByIDPreparedStatement;
    private final PreparedStatement getProjectsByCategoryPreparedStatement;
    private final PreparedStatement setProjectByPreparedStatement;
    private final PreparedStatement setCategoryProjectByPreparedStatement;
    private final PreparedStatement setUsernameProjectByPreparedStatement;
    private final PreparedStatement setBackingUser;
    private final PreparedStatement getProjectCountersPreparedStatement;
    private final ExecutorService executor;

    public ProjectDAO(List<String> contactPoints, String keyspace) {

        cluster = Cluster.builder().addContactPoints(
                contactPoints.toArray(new String[contactPoints.size()])).withRetryPolicy(Policies.defaultRetryPolicy()).build();

        session = cluster.connect(keyspace);

        getProjectsByUsernamePreparedStatement = session.prepare(GET_PROJECTS_BY_USERNAME);
        getProjectByIDPreparedStatement = session.prepare(GET_PROJECT_BY_ID);
        getProjectsByCategoryPreparedStatement = session.prepare(GET_PROJECTS_BY_CATEGORY);
        getProjectCountersPreparedStatement = session.prepare(GET_COUNTERS_BY_PROJECT);
        setProjectByPreparedStatement = session.prepare(SET_PROJECT);
        setCategoryProjectByPreparedStatement = session.prepare(SET_CATEGORY_PROJECTS_INDEX);
        setUsernameProjectByPreparedStatement = session.prepare(SET_USERNAME_PROJECTS_INDEX);

        setBackingUser = session.prepare(SET_BACKING_USER);


        // Create a thread pool equal to the number of cores
        executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    }

    @Override
    public void close() {
        cluster.shutdown();
    }

    /**
     * Using QueryBuilder is the most secure way of creating a CQL query. Avoids
     * issues with injection style attacks.
     *
     * @param projectId
     * @return
     */
    @Override
    public Project getprojectByIdUsingQueryBuilder(String projectid) {

        Project project = new Project();

        Query query = select().all().from("byb", "projects").where(eq("projectid", UUID.fromString(projectid))).limit(10);

        // We can change our Consistency Level on the fly, per read or write.
        query.setConsistencyLevel(ConsistencyLevel.ONE);

        // We can also enable tracing on this query
        query.enableTracing();

        ResultSet rs = session.execute(query);

        // If you want the trace information, you can use the following method
        // QueryTrace queryTrace = rs.getExecutionInfo().getQueryTrace();

        for (Row row : rs) {

            project.setProjectid(row.getUUID("projectid"));
            project.setProjectname(row.getString("projectName"));
            project.setUsername(row.getString("username"));
            project.setLocation(row.getString("location"));
            project.setDescription(row.getString("description"));
            project.setCategory(row.getString("category"));
            project.setBudjet(row.getInt("budjet"));
            project.setMinimum_pledge(row.getInt("minimum_pledge"));
            // This is a get of a CQL List collection. You must specify
            // the type of the map. In this case String for varchar and Integer for int
            project.setBackers(row.getMap("backers", String.class, Integer.class));
            project.setLaunched_date(row.getDate("launched_date"));
            project.setFunding_end_date(row.getDate("funding_end_date"));
        }

        return project;

    }

    /**
     * A powerful and efficient way of reading data from your cluster. When you
     * need more than one row, consider using an AsyncRead with a
     * ResultSetFuture Each query is done in parallel and is non-blocking until
     * you issue the get.
     *
     * @param username
     * @return
     */
    @Override
    public List<Project> getProjectsByUsernameUsingAsyncRead(String username) {

        // Get a list of projectIds for one user from username
        BoundStatement bs = getProjectsByUsernamePreparedStatement.bind();

        // We'll create a List of futures for each query we need to run.
        List<ResultSetFuture> futures = new ArrayList<ResultSetFuture>();
        List<Project> projects = new ArrayList<Project>();

        bs.setString("username", username);

        // First we will grab a list of projectIds
        for (Row row : session.execute(bs)) {

            // For each projectId we will create a query to get each project
            // As each is created, they are executed in the background
            futures.add(session.executeAsync(getProjectByIDPreparedStatement.bind(row.getUUID("projectid"))));
        }

        for (ResultSetFuture future : futures) {

            // getUninterruptibly() is used when we don't mind the
            // threads being interrupted. The option is to just use
            // get and catch the exception
            for (Row row : future.getUninterruptibly()) {
                Project project = new Project();
                project.setProjectid(row.getUUID("projectid"));
                project.setProjectname(row.getString("projectName"));
                project.setUsername(row.getString("username"));
                project.setLocation(row.getString("location"));
                project.setDescription(row.getString("description"));
                project.setCategory(row.getString("category"));
                project.setBudjet(row.getInt("budjet"));
                project.setMinimum_pledge(row.getInt("minimum_pledge"));
                // This is a get of a CQL List collection. You must specify
                // the type of the map. In this case String for varchar and Integer for int
                project.setBackers(row.getMap("backers", String.class, Integer.class));
                project.setLaunched_date(row.getDate("launched_date"));
                project.setFunding_end_date(row.getDate("funding_end_date"));

                projects.add(project);
            }
        }

        return projects;
    }

    /**
     * This is a bit more of a complicated example using AsyncRead and threads.
     * The threads are used to background a query and notify when complete.
     * After all queries are run, we use a latch wait for the last result to be
     * returned.
     *
     * @param categorys
     * @return
     */
    @Override
    public List<Project> getProjectsByCategorysUsingAsyncReadThreads(List<String> categorys) {

        List<Project> projects = new ArrayList<Project>();

        // ArrayList is not thread safe so not good for concurrent
        // In this case we will ensure it is synchronized
        final List<ResultSetFuture> projectFutures = Collections.synchronizedList(new ArrayList<ResultSetFuture>());

        // Latch for holding until the last query is returned
        final CountDownLatch latch = new CountDownLatch(categorys.size());

        // ConcurrentHashMap will be used to create a unique list of projectIds
        final ConcurrentHashMap<UUID, Object> projectIds = new ConcurrentHashMap<UUID, Object>();

        for (String tag : categorys) {
            BoundStatement bs = getProjectsByCategoryPreparedStatement.bind(tag);
            final ResultSetFuture future = session.executeAsync(bs);

            // For each projectId in the tag table, we create a background thread
            // to retrieve the project details. Those futures are put in a hash to
            // eliminate
            // duplicates in our final list.
            future.addListener(new Runnable() {

                @Override
                public void run() {
                    for (Row row : future.getUninterruptibly()) {
                        UUID projectId = row.getUUID("projectid");

                        // Create a non-duplicate hash map of project IDs
                        if (projectIds.putIfAbsent(projectId, PRESENT) == null) {

                            projectFutures.add(session.executeAsync(getProjectByIDPreparedStatement.bind(projectId)));
                        }
                    }
                    latch.countDown();
                }
            }, executor);
        }

        // Wait for each thread to finish
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (ResultSetFuture future : projectFutures) {

            // For each future we now will grab the result set of each project
            for (Row row : future.getUninterruptibly()) {
                Project project = new Project();
                project.setProjectid(row.getUUID("projectid"));
                project.setProjectname(row.getString("projectName"));
                project.setUsername(row.getString("username"));
                project.setLocation(row.getString("location"));
                project.setDescription(row.getString("description"));
                project.setCategory(row.getString("category"));
                project.setBudjet(row.getInt("budjet"));
                project.setMinimum_pledge(row.getInt("minimum_pledge"));
                // This is a get of a CQL List collection. You must specify
                // the type of the map. In this case String for varchar and Integer for int
                project.setBackers(row.getMap("backers", String.class, Integer.class));
                project.setLaunched_date(row.getDate("launched_date"));
                project.setFunding_end_date(row.getDate("funding_end_date"));

                projects.add(project);
            }
        }

        return projects;
    }

    @Override
    public List<Project> getProjectsByCategoryUsingAsyncRead(String category) {

        // Get a list of projectIds for one user from username
        BoundStatement bs = getProjectsByCategoryPreparedStatement.bind(category);
        List<ResultSetFuture> futures = new ArrayList<ResultSetFuture>();
        List<Project> projects = new ArrayList<Project>();

        for (Row row : session.execute(bs)) {

            futures.add(session.executeAsync(getProjectByIDPreparedStatement.bind(row.getUUID("projectid"))));
        }

        for (ResultSetFuture future : futures) {

            // Uninterrupted get
            for (Row row : future.getUninterruptibly()) {
                Project project = new Project();
                project.setProjectid(row.getUUID("projectid"));
                project.setProjectname(row.getString("projectName"));
                project.setUsername(row.getString("username"));
                project.setLocation(row.getString("location"));
                project.setDescription(row.getString("description"));
                project.setCategory(row.getString("category"));
                project.setBudjet(row.getInt("budjet"));
                project.setMinimum_pledge(row.getInt("minimum_pledge"));
                // This is a get of a CQL List collection. You must specify
                // the type of the map. In this case String for varchar and Integer for int
                project.setBackers(row.getMap("backers", String.class, Integer.class));
                project.setLaunched_date(row.getDate("launched_date"));
                project.setFunding_end_date(row.getDate("funding_end_date"));

                projects.add(project);
            }

        }

        return projects;
    }

    @Override
    public Project setProjectByPreparedStatement(Project project) {

        BoundStatement bs = setProjectByPreparedStatement.bind();
        BoundStatement bsC = setCategoryProjectByPreparedStatement.bind();
        BoundStatement bsU = setUsernameProjectByPreparedStatement.bind();
        UUID uuid = UUID.randomUUID();
        Date currentDate = Calendar.getInstance().getTime();

        bs.setUUID("projectid", uuid);
        project.setProjectid(uuid);
        bs.setString("projectname", project.getProjectname());
        bs.setString("username", project.getUsername());
        bs.setString("location", project.getLocation());
        bs.setString("description", project.getDescription());
        bs.setString("category", project.getCategory());

        bs.setDate("launched_date", currentDate);
        project.setLaunched_date(currentDate);

        bs.setDate("funding_end_date", project.getFunding_end_date());
        bs.setInt("budjet", project.getBudjet());
        bs.setInt("minimum_pledge", project.getMinimum_pledge());

        bsC.setString("category", project.getCategory());
        bsC.setUUID("projectid", uuid);

        bsU.setString("username", project.getUsername());
        bsU.setUUID("projectid", uuid);
        bsU.setDate("launched_date", currentDate);


        session.execute(bs);
        session.execute(bsU);
        session.execute(bsC);

        return project;
    }

    @Override
    public long getProjectCounters(Project project) {
        BoundStatement bs = getProjectCountersPreparedStatement.bind(project.getProjectid());

        ResultSet rs = session.execute(bs);

        Row row = rs.one();

        if (row == null) {
            return 0;
        }
        // Get the count and total rating for the project
        long views = row.getLong("views_counter");
        long comments = row.getLong("comments_counter");

        // Divide the total by the count and return an average
        return (comments * 10 + views);
    }

    @Override
    public void setViewsForProject(UUID projectid) {

        // Very simple way of incrementing a count
        session.execute("UPDATE project_counters SET views_counter = views_counter + 1 WHERE projectid = " + projectid);

    }

    @Override
    public void setCommentsForProject(UUID projectid) {

        // Very simple way of incrementing a count
        session.execute("UPDATE project_counters SET comments_counter = comments_counter + 1 WHERE projectid = " + projectid);

    }

    @Override
    public void setBacking(UUID projectid, String username, int amount) {

        BoundStatement backingUser = setBackingUser.bind();

        backingUser.setString("username", username);
        backingUser.setUUID("projectid", projectid);
        backingUser.setInt("pledge_amount", amount);

        StringBuffer backingProject = new StringBuffer(
                "UPDATE projects SET backers[");
        backingProject.append("'" + username + "'");
        backingProject.append("]=" + amount + " ");
        backingProject.append("WHERE projectid=" + projectid.toString());
        backingProject.append(";");
        
        session.execute(backingUser);
        session.execute(backingProject.toString());
    }
}
