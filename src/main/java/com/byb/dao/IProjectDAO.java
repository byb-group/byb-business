/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.byb.dao;

import com.byb.model.Project;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author hadoop
 */
public interface IProjectDAO {

    void close();

    long getProjectCounters(Project project);

    List<Project> getProjectsByCategoryUsingAsyncRead(String category);

    /**
     * This is a bit more of a complicated example using AsyncRead and threads.
     * The threads are used to background a query and notify when complete.
     * After all queries are run, we use a latch wait for the last result to be
     * returned.
     *
     * @param categorys
     * @return
     */
    List<Project> getProjectsByCategorysUsingAsyncReadThreads(List<String> categorys);

    /**
     * A powerful and efficient way of reading data from your cluster. When you
     * need more than one row, consider using an AsyncRead with a
     * ResultSetFuture Each query is done in parallel and is non-blocking until
     * you issue the get.
     *
     * @param username
     * @return
     */
    List<Project> getProjectsByUsernameUsingAsyncRead(String username);

    /**
     * Using QueryBuilder is the most secure way of creating a CQL query. Avoids
     * issues with injection style attacks.
     *
     * @param projectId
     * @return
     */
    Project getprojectByIdUsingQueryBuilder(String projectid);

    void setBacking(UUID projectid, String username, int amount);

    void setCommentsForProject(UUID projectid);

    Project setProjectByPreparedStatement(Project project);

    void setViewsForProject(UUID projectid);
    
}
