package com.byb.model;

import java.util.Date;

public class User {

	public User() {
		super();

	}

	public User(String username, String firstname, String lastname,
			String email, String password, Date created_date,
			String location) {
		super();
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.created_date = created_date;
		this.location = location;
	}

	/**
	 * @param args
	 */

	private String username;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private Date created_date;
	private String location;
        private String biography;

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreated_date() {
		return created_date;
	}

	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
        
            public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public String toString() {
        return "User{" + "username=" + username + ", firstname=" + firstname + ", lastname=" + lastname + ", email=" + email + ", created_date=" + created_date + ", location=" + location + '}';
    }



}
