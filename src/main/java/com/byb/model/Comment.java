package com.byb.model;

import java.util.Date;
import java.util.UUID;

public class Comment {

    UUID projectid;
    String username;
    Date comment_ts;
    String comment;
    String projectName;

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Comment() {
    }

    public UUID getProjectid() {
        return projectid;
    }

    public void setProjectid(UUID projectid) {
        this.projectid = projectid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getComment_ts() {
        return comment_ts;
    }

    public void setComment_ts(Date comment_ts) {
        this.comment_ts = comment_ts;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Comment [videoid=" + projectid + ", username=" + username
                + ", comment_ts=" + comment_ts + ", comment=" + comment + "]";
    }
}
