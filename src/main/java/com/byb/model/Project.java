package com.byb.model;

import java.util.Date;
import java.util.Map;
import java.util.UUID;
import org.joda.time.DateTime;
import org.joda.time.Days;

public class Project {

//Project info 	
    private UUID projectid;
    private String projectname;
    private String description;
    private String category;
    private Date launched_date;
    private Date funding_end_date;
    private int budjet;
    private int minimum_pledge;
    //private List<String> tags;
//Creator info
    private String username;
    private String location;
    //Backers info 
    Map<String, Integer> backers;
    //Counter info
    int views;
    int comments;

    public int getComments() {
        return comments;
    }

    public void setComments(int comments) {
        this.comments = comments;
    }

    public int getViews() {
        return views;
    }

    public void setViews(int views) {
        this.views = views;
    }

    public UUID getProjectid() {
        return projectid;
    }

    public void setProjectid(UUID projectid) {
        this.projectid = projectid;
    }

    public String getProjectname() {
        return projectname;
    }

    public void setProjectname(String projectname) {
        this.projectname = projectname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBudjet() {
        return budjet;
    }

    public void setBudjet(int budjet) {
        this.budjet = budjet;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Date getFunding_end_date() {
        return funding_end_date;
    }

    public void setFunding_end_date(Date funding_end_date) {
        this.funding_end_date = funding_end_date;
    }

    public Date getLaunched_date() {
        return launched_date;
    }

    public void setLaunched_date(Date launched_date) {
        this.launched_date = launched_date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getMinimum_pledge() {
        return minimum_pledge;
    }

    public void setMinimum_pledge(int minimum_pledge) {
        this.minimum_pledge = minimum_pledge;
    }

    public Map<String, Integer> getBackers() {
        return backers;
    }

    public void setBackers(Map<String, Integer> backers) {
        this.backers = backers;

    }

    public int getPledge() {
        
        int pledge = 0;
        if(backers!=null && backers.size()>0){
            for (Integer i : backers.values()) {
                pledge+=i;
            }
        }

        return pledge;
    }

    public int getPercentage() {
        Float p = new Float((getPledge() / budjet) * 100);
        return p.intValue();
    }

    public int getDayLeft() {
        return Days.daysBetween(new DateTime(launched_date), new DateTime(funding_end_date)).getDays();
    }

    @Override
    public String toString() {
        return "Project{" + "projectname=" + projectname + ", category=" + category + ", budjet=" + budjet + ", username=" + username + ", location=" + location + '}';
    }

    public void addBacker(String username, int amount) {
        backers.put(username, amount);
    }
}
